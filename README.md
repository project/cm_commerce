# CM Payment gateway

[TOC]

## Introduction
Payment gateway for CM.com

## Description
This module provides a payment gateway module for [CM](https://www.cm.com/) for Drupal. It uses the 'menu' option where
the choice for the payment provider (Credit card, iDeal, PayPal etc) is handled by the CM API.

- [CM API Docs](https://www.cm.com/en/app/docs)
- [CM Payment Portal](https://services.docdatapayments.com/portal/)

## Requirements
- commerce
- commerce_payment

## Installation
See [installing modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for installation instructions.
