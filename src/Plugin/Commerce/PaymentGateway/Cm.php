<?php

namespace Drupal\cm_commerce\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\Error;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the CM offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "cm",
 *   label = @Translation("CM (Redirect to CM)"),
 *   display_label = @Translation("CM"),
 *    forms = {
 *     "offsite-payment" = "Drupal\cm_commerce\PluginForm\RedirectCheckoutForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa",
 *   },
 * )
 */
class Cm extends OffsitePaymentGatewayBase {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    // We need to define httpClient here for capture/void/refund operations,
    // as it is not passed to off-site plugins constructor.
    $this->httpClient = new Client();
  }

  /**
   * Sets up default configuration.
   *
   * @return array
   *   Config array.
   */
  public function defaultConfiguration() {
    return [
      'merchant_name' => '',
      'password' => '',
      'merchant_key' => '',
      'payment_method' => '',
      'accepted_cards' => '',
      'debug_info' => 0,
        // 'return_url_success' => '',
        // 'return_url_canceled' => '',
        // 'return_url_pending' => '',
        // 'return_url_error' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['merchant_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant name'),
      '#description' => $this->t('This is the merchant name for your CM account.'),
      '#default_value' => $this->configuration['merchant_name'],
      '#required' => TRUE,
    ];

    $form['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#description' => $this->t('The password as provided by CM.'),
      '#default_value' => $this->configuration['password'],
      '#required' => TRUE,
    ];

    $form['merchant_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant key'),
      '#description' => $this->t('The Merchant key for the merchant as provided by CM.'),
      '#default_value' => $this->configuration['merchant_key'],
      '#required' => TRUE,
    ];

    $form['debug_info'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug'),
      '#description' => $this->t('Adds debug information to the log.'),
      '#default_value' => $this->configuration['debug_info'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['merchant_name'] = $values['merchant_name'];
    $this->configuration['password'] = $values['password'];
    $this->configuration['merchant_key'] = $values['merchant_key'];
    $this->configuration['debug_info'] = $values['debug_info'];
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    parent::onReturn($order, $request);

    // If we don't update payment info here it is not processed on time!
    try {
      $payment = $this->updatePaymentInfo($request->query->get('order_reference'));
    }
    catch (EntityStorageException | GuzzleException $e) {
      Error::logException(\Drupal::logger('cm'), $e);
    }

    // Log the response message if request logging is enabled.
    if (isset($this->configuration['debug_info']) && $this->configuration['debug_info']) {
      \Drupal::logger('cm_commerce')
        ->debug('CM on return response: <pre>@body</pre>', [
          '@body' => var_export($request->query->all(), TRUE),
        ]);
    }

    // Common response processing for both redirect back and async notification.
    // $payment = $this->processFeedback($request);
    // Do not update payment state here - it should be done from the received
    // notification only, and considering that usually notification is received
    // even before the user returns from the off-site redirect, at this point
    // the state tends to be already the correct one.
    if (isset($payment) && $payment->get('state')
      ->getString() !== 'completed') {
      throw new PaymentGatewayException('Payment state is not completed.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    parent::onCancel($order, $request);

    // Display a message with a link to the payment gateway.
    $this->messenger()->addMessage(
      $this->t('You have canceled checkout at @gateway but may resume the checkout process here when you are ready!',
        [
          '@gateway' => $this->getDisplayLabel(),
        ]));
  }

  /**
   * Processes the notification request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   */
  public function onNotify(Request $request) {
    parent::onNotify($request);

    if (isset($this->configuration['debug_info']) && $this->configuration['debug_info']) {

      \Drupal::logger('cm_commerce')
        ->debug('CM config: <pre>@body</pre>', [
          '@body' => var_export($this->configuration, TRUE),
        ]);

      // Log the response message if request logging is enabled.
      \Drupal::logger('cm_commerce')
        ->debug('CM update notification: <pre>@body</pre>', [
          '@body' => var_export($request->query->all(), TRUE),
        ]);
    }

    try {
      $payment = $this->updatePaymentInfo($request->query->get('order_ref'));
      if (isset($this->configuration['debug_info']) && $this->configuration['debug_info']) {
        \Drupal::logger('cm_commerce')
          ->debug('CM update notification remote id: <pre>@body</pre>', [
            '@body' => var_export($payment->getRemoteId(), TRUE),
          ]);
      }
    }
    catch (EntityStorageException | GuzzleException $e) {
      Error::logException(\Drupal::logger('cm'), $e);
    }
  }

  /**
   * Checks the status of the payment and updates the payment object.
   *
   * @param string $encodedOrderReference
   *   The encoded order reference.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  private function updatePaymentInfo(string $encodedOrderReference): PaymentInterface|bool {
    // Load the payment entity created in
    // RedirectCheckoutForm::buildConfigurationForm().
    $order_ref = str_replace($this->configuration['merchant_key'] . $this->configuration['mode'], '', base64_decode($encodedOrderReference));
    $order_ref = preg_replace('/%.*?%/', '', $order_ref);

    try {
      $payment = $this->entityTypeManager->getStorage('commerce_payment')
        ->load($order_ref);
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      Error::logException(\Drupal::logger('cm'), $e);
    }

    // Added check if the state is already completed, so we do not update the
    // payment state again.
    if (isset($payment) && $payment->getState()->getId() !== 'completed') {
      // Common response processing for both redirect back and async
      // notification.
      $payment = $this->processFeedback($payment->getRemoteId(), $payment);
      $payment->save();
    }

    return $payment ?? FALSE;
  }

  /**
   * Get the status of an order from CM.
   *
   * @param string $order_ref
   *   The order reference.
   * @param \Drupal\commerce_payment\Entity\Payment $payment
   *   The payment entity.
   *
   * @return \Drupal\commerce_payment\Entity\Payment
   *   The updated payment entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function processFeedback(string $order_ref, Payment $payment): Payment {
    // GET /ps/api/public/v1/merchants/{merchant_key}/orders/{order_key}.
    $api_url = $this->getApiUrl();

    $url = $api_url . 'merchants/' . $this->configuration['merchant_key'] . '/orders/' . $order_ref;

    $options = [
      'auth' => [
        $this->configuration['merchant_name'],
        $this->configuration['password'],
      ],
      'absolute' => TRUE,
    ];

    $client = \Drupal::httpClient();
    $request = $client->request('GET', $url, $options);
    $response = $request->getBody()->getContents();
    $response = json_decode($response);

    if (isset($this->configuration['debug_info']) && $this->configuration['debug_info']) {
      // Log the response message if request logging is enabled.
      \Drupal::logger('cm_commerce_feedback')
        ->debug('CM feedback: <pre>@body</pre>', [
          '@body' => var_export($response, TRUE),
        ]);
    }

    if (isset($response->payments[0]->authorization->state) &&
      $response->payments[0]->authorization->state == 'AUTHORIZED') {
      $isAuthorized = $payment->getAuthorizedTime();
      if (empty($isAuthorized)) {
        $payment->setAuthorizedTime(time());
      }
    }

    if (isset($response->considered_safe->level) &&
      $response->considered_safe->level == 'SAFE') {
      $order = Order::load($response->description);
      $paymentId = $response->payments[0]->id;
      $paymentId = preg_replace('/[^0-9]/', '', $paymentId);
      $order->set('field_cm_order_id', $response->order_reference);
      $order->set('field_cm_payment_id', $paymentId);
      $order->save();
      $payment->set('state', 'completed');
      return $payment;
    }

    $payment->set('state', 'new');

    return $payment;
  }

  /**
   * Check if the environment is set to test or not.
   *
   * @returns string
   *   Returns the correct string depending on the environment.
   */
  public function getApiUrl(): string {
    if ($this->getMode() == 'test') {
      return 'https://testsecure.docdatapayments.com/ps/api/public/v1/';
    }
    else {
      return 'https://secure.docdatapayments.com/ps/api/public/v1/';
    }
  }

}
