<?php

namespace Drupal\cm_commerce\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use GuzzleHttp\Exception\RequestException;

/**
 * Offsite payment gateway.
 */
class RedirectCheckoutForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $configuration = $this->getConfiguration();

    /**
     * @var \Drupal\commerce_payment\Entity\PaymentInterface $payment
     */
    $payment = $this->entity;

    // Save the payment entity so that we can get its ID and use it for
    // building the 'ORDERID' property for CM. Then, when user returns
    // from the off-site redirect, we will update the same payment.
    $payment->save();

    // API version.
    $data['version'] = 'v1';

    // Merchant name.
    $data['merchant_name'] = $configuration['merchant_name'];

    // Currency code.
    $data['currency'] = $payment->getAmount()->getCurrencyCode();

    // Add payment gateway id and internal order_id as custom variables.
    // Used in the callback method.
    $data['variables[payment_gateway]'] = $payment->getPaymentGatewayId();
    $data['variables[order]'] = $payment->getOrderId();

    $success_url = Url::FromRoute('commerce_payment.checkout.return', [
      'commerce_order' => $payment->getOrderId(),
      'step' => 'payment',
    ], ['absolute' => TRUE])->toString();

    $cancel_url = Url::FromRoute('commerce_payment.checkout.cancel', [
      'commerce_order' => $payment->getOrderId(),
      'step' => 'payment',
    ], ['absolute' => TRUE])->toString();

    $order = $payment->getOrder();
    $billing_profile = $order->billing_profile->first()->entity;
    $billing_address = $billing_profile->get('address')->first()->getValue();
    \Drupal::logger('cm_commerce')
      ->debug('Billing address: <pre>@body</pre>', [
        '@body' => var_export($billing_address, TRUE),
      ]);

    $shopperOptions = [
      'url' => [
        'url' => $this->getApiUrl(),
        'version' => $data['version'],
        'merchant_key' => $configuration['merchant_key'],
      ],
      'auth' => [
        'merchant_name' => $configuration['merchant_name'],
        'password' => $configuration['password'],
      ],
      'json' => [
        'shopper_reference' => $billing_profile->id(),
        'name' => [
          'first' => $billing_address['given_name'],
          'last' => $billing_address['family_name'],
        ],
        'address' => [
          'street' => $billing_address['address_line1'],
          'housenumber' => '1',
          'postal_code' => $billing_address['postal_code'],
          'city' => $billing_address['locality'],
          'country' => $billing_address['country_code'],
        ],
        'email' => $order->getEmail(),
      ],
    ];

    $shopper = $this->pushShopper($shopperOptions);
    // Prepare some data so we can push the order to CM
    // to get a url for further processing.
    $options = [
      'url' => [
        'url' => $this->getApiUrl(),
        'version' => $data['version'],
        'merchant_key' => $configuration['merchant_key'],
      ],
      'auth' => [
        'merchant_name' => $configuration['merchant_name'],
        'password' => $configuration['password'],
      ],
      'json' => [
        'order_reference' => base64_encode($payment->id() . '%' . $payment->getOrderId() . '%' . $configuration['merchant_key'] . $configuration['mode']),
        // Encoded to prevent duplicate orders.
        'description' => $payment->getOrderId(),
        'amount' => round($payment->getAmount()->getNumber() * 100, 0),
        'currency' => $data['currency'],
        'email' => $payment->getOrder()->getEmail(),
        'language' => 'nl',
        'country' => 'NL',
        'billing_address_key' => $shopper->address_key,
        'return_urls' => [
          'success' => $success_url . '?order_reference=',
          'canceled' => $cancel_url . '?order_reference=',
          'pending' => $success_url . '?order_reference=',
          'error' => $success_url . '?order_reference=',
        ],
      ],
    ];
    $cm_order_data = $this->pushOrder($options);

    $payment->setRemoteId($cm_order_data->order_key);
    $payment->save();

    if (isset($configuration['debug_info']) && $configuration['debug_info']) {
      \Drupal::logger('cm_commerce')
        ->debug('Succes URL: <pre>@body</pre>', [
          '@body' => $success_url,
        ]);
    }

    if (isset($configuration['debug_info']) && $configuration['debug_info']) {
      \Drupal::logger('cm_commerce')
        ->debug('CM Payment id, ref: <pre>@body</pre>', [
          '@body' => var_export($payment->id(), TRUE),
        ]);
    }

    return $this->buildRedirectForm(
      $form,
      $form_state,
      $cm_order_data->url,
      // Data.
      [],
      PaymentOffsiteForm::REDIRECT_GET
    );
  }

  /**
   * Get configuration.
   *
   * @return array
   *   Configuration array.
   */
  private function getConfiguration() {
    /**
     * @var \Drupal\commerce_payment\Entity\PaymentInterface $payment
     */
    $payment = $this->entity;

    /**
     * @var \Drupal\cm_commerce\Plugin\Commerce\PaymentGateway\RedirectCheckout $payment_gateway_plugin
     */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

    return $payment_gateway_plugin->getConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveEndpointUrl() {
    return $this->getMode() == 'test' ? 'https://testsecure.docdatapayments.com' : 'https://secure.docdatapayments.com';
  }

  /**
   * Post order details to CM to get a url to payment gateway.
   *
   * @return Object
   *   Object containing url.
   */
  private function pushOrder($options) {
    $client = \Drupal::httpClient();

    try {
      $request = $client->post(
        $options['url']['url'] . '/ps/api/public/' . $options['url']['version'] . '/merchants/' . $options['url']['merchant_key'] . '/orders', [
          'auth' => [
            $options['auth']['merchant_name'],
            $options['auth']['password'],
          ],
          'json' => $options['json'],
        ]
      );
      $response = json_decode($request->getBody());

      // Needed to fetch updates.
      $order_ref = $response->order_key;

      if (isset($configuration['debug_info']) && $configuration['debug_info']) {
        \Drupal::logger('cm_commerce')
          ->debug('CM order pushed: <pre>@body</pre>', [
            '@body' => var_export($response, TRUE),
          ]);
        \Drupal::logger('cm_commerce')
          ->debug('CM order pushed, ref: <pre>@body</pre>', [
            '@body' => var_export($order_ref, TRUE),
          ]);
      }
      return $response;
    }
    catch (RequestException $e) {
      Error::logException(\Drupal::logger('cm_commerce'), $e);
    }
  }

  /**
   * Post shopper details to CM to create a shopper in the payment gateway.
   *
   * @return Object
   *   Object containing shopper key and address key, which we'll use later.
   */
  private function pushShopper($options) {
    $client = \Drupal::httpClient();
    try {
      $request = $client->post(
        $options['url']['url'] . '/ps/api/public/' . $options['url']['version'] . '/merchants/' . $options['url']['merchant_key'] . '/shoppers', [
          'auth' => [
            $options['auth']['merchant_name'],
            $options['auth']['password'],
          ],
          'json' => $options['json'],
        ]
      );
      $response = json_decode($request->getBody());

      if (!empty($configuration['debug_info'])) {
        \Drupal::logger('cm_commerce')
          ->debug('Error push shopper: <pre>@body</pre>', [
            '@body' => var_export($response, TRUE),
          ]);

      }
      return $response;
    }
    catch (RequestException $e) {
      Error::logException(\Drupal::logger('cm_commerce'), $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getApiUrl() {
    $configuration = $this->getConfiguration();

    if ($configuration['mode'] == 'test') {
      return 'https://testsecure.docdatapayments.com';
    }
    else {
      return 'https://secure.docdatapayments.com';
    }
  }

}
